#!/usr/bin/env python3

import argparse
import hashlib
import itertools
import os
import re
import shutil
import sys
import time
import uno
import yaml

from com.sun.star.text.WrapTextMode import NONE as TextWrapNone
from localize import translate as _translate
from pathlib import Path
from PIL import Image
from thirdparty import dictToProperties, signal_last, convertPathToOOPath, openDocument
from unohelper import systemPathToFileUrl, absolutize


ARGS = None
SCRIPTDIR = os.path.dirname(os.path.realpath(__file__))

class LODoc:
    def __init__(self, doc):
        self.doc = doc
        self.dispatcher = uno.getComponentContext().getServiceManager().createInstance('com.sun.star.frame.DispatchHelper')
        self.document = doc.getCurrentController().getFrame()

    def dispose(self):
        self.doc.dispose()

    #def __del__(self):
    #    self.dispose()

    def executeDispatch(self, name, propdict={}):
        time.sleep(.05)
        prop = dictToProperties(propdict)
        self.dispatcher.executeDispatch(self.document, ".uno:" + name, "", 0, prop)

    def saveinsert_bookmark(self, bookmark):
        bookmarks = self.doc.getBookmarks()
        while bookmark not in bookmarks.getElementNames():
            self.executeDispatch("InsertBookmark", {'Bookmark': bookmark})

    def savedelete_bookmark(self, bookmark):
        self.executeDispatch("DeleteBookmark", {'Bookmark': bookmark})

    def get_all_bookmarks_starts_with(self, string):
        return [ bm for bm in self.doc.getBookmarks().getElementNames() if bm.startswith(string) ]

    def savedelete_all_bookmarks(self):
        bookmarks = self.doc.getBookmarks()
        time.sleep(.5) # LO crashes in some cases if the deletion is initiated too early -.-
        for bookmark in bookmarks.getElementNames():
            self.savedelete_bookmark(bookmark)

    def lo_insert_text(self, text, do_not_rstrip=False):
        if do_not_rstrip:
            text = str(text)
        else:
            text = str(text).rstrip('\n')

        self.executeDispatch("InsertText", {"Text": text})

    def lo_gen_anchor(self, anchor_text, anchor_id):
        """
        Generate an anchor with given anchor_text and anchor_id
        """
        # add a no-width optional break
        # this will server as a non-spaceconsuming delimiter between
        # the anchor-text and the potential following non-anchor text
        self.executeDispatch("InsertZWSP")
        c1sf = { 'Count': 1, 'Select': False }
        c1st = { 'Count': 1, 'Select': True }
        self.executeDispatch("GoLeft", c1sf)

        # Place the first char of the anchor_text an select it
        self.lo_insert_text(anchor_text[0])

        self.executeDispatch("GoLeft", c1st)

        # convert this character to an anchor
        self.executeDispatch("InsertField", { 'Type': 12, 'Subtype': 0, 'Name': anchor_id })

        # place the cursor on the rhs of the character und add place the rest of 'anchor_text'
        self.executeDispatch("GoRight", c1sf)
        self.lo_insert_text(anchor_text[1:])

        # escape anchor-text by putting the cursor beyond the previously created no-width optional break
        self.executeDispatch("GoRight", c1sf)

    def lo_gen_anchor_ref(self, anchor_id):
        self.executeDispatch("InsertField", {
            'Type': 13,
            'Subtype': 0,
            'Format': 2,
            'Name': anchor_id,
            })


    def lo_search_for(self, searchstring):
        self.executeDispatch("ExecuteSearch", {
            'SearchItem.SearchString': searchstring,
            'SearchItem.StyleFamily': 2})


    def lo_search_and_replace(self, searchstring, replacestring):
        self.lo_search_for(searchstring)
        self.lo_insert_text(replacestring)


    def jump_to_next_tablecell_save(self):
        tablecell_at_start = self.doc.getCurrentSelection().getByIndex(0).Cell.CellName
        self.executeDispatch("JumpToNextCell")
        while self.doc.getCurrentSelection().getByIndex(0).Cell.CellName == tablecell_at_start:
            time.sleep(.1)
        return self.doc.getCurrentSelection().getByIndex(0).Cell.CellName


    def jump_to_bookmark(self, bookmark):
        if bookmark not in self.doc.getBookmarks().getElementNames():
            raise Exception("Missing bookmark '%s'" % bookmark)
        self.executeDispatch("JumpToMark", {'Bookmark': bookmark})


    def jump_to_bookmark_if_it_exists(self, bookmark):
        if bookmark not in self.doc.getBookmarks().getElementNames():
            return False
        self.executeDispatch("JumpToMark", {'Bookmark': bookmark})
        return True


    def go_to_end_of_doc(self):
        # unfortunately dispatcher commands GoToEndOfDoc fails sometimes
        self.executeDispatch("GoToStartOfDoc")
        self.executeDispatch("SelectAll")
        self.executeDispatch("GoRight")


    def bugfix_update_layout(self):
        """
        Tables normally adjust their size to not collide with frames.
        When frames are vertically growing into the space of an existing table
        the table size seem not to be updated reliably.
        To fix this, it is sufficient to change the Fieldnames view.
        """
        self.toggle_fieldnames_view()


    def toggle_fieldnames_view(self):
        self.executeDispatch("Fieldnames", {"Fieldnames": True})
        self.executeDispatch("Fieldnames", {"Fieldnames": False})


    def put_graphic_on_bookmarklocation(self, filename, bookmark):
        self.executeDispatch("JumpToMark", {'Bookmark': bookmark})
        self.bugfix_update_layout()
        old_set_of_graphinc = set(self.doc.getGraphicObjects().ElementNames)
        new_image = ""
        while not new_image:
            self.executeDispatch("InsertGraphic", {'FileName': filename})
            new_set_of_graphinc = set(self.doc.getGraphicObjects().ElementNames)
            set_diff = new_set_of_graphinc - old_set_of_graphinc
            if not set_diff:
                time.sleep(.1)
            else:
                new_image = list(set_diff)[0]
        self.doc.getGraphicObjects().getByName(new_image).TextWrap = TextWrapNone
        return(new_image)


    def delete_elements_with_nameprefixes(self, elementtype, nameprefixes):
        if isinstance(nameprefixes, str):
            nameprefixes = [nameprefixes]
        doc = self.doc
        if elementtype == 'table':
            elementnames = doc.getTextTables().getElementNames()
        elif elementtype == 'frame':
            elementnames = doc.getTextFrames().getElementNames()
        else:
            raise Exception("Unknown Element type %s" % elementtype)

        def name_matches_one_of_prefixes(name, prefixes):
            for prefix in prefixes:
                if name.startswith(prefix):
                    return(True)
            return(False)

        for elementname in elementnames:
            if name_matches_one_of_prefixes(elementname, nameprefixes):
                self.delete_element(elementtype, elementname)


    def delete_all_elements_with_nameprefixes(self, nameprefixes):
        for elementtype in ['table', 'frame']:
            self.delete_elements_with_nameprefixes(elementtype, nameprefixes)


    def delete_element(self, elementtype, elementname):
        doc = self.doc
        if elementtype == 'table':
            def get_all_existing_elements():
                return(doc.getTextTables().getElementNames())
            def get_selected_elementname():
                return(doc.getCurrentSelection().getByIndex(0).TextTable.getName())
            def jump_to_next_element():
                self.executeDispatch("JumpToNextTable")
            def delete_selected_element():
                self.executeDispatch("DeleteTable")
        elif elementtype == 'frame':
            def get_all_existing_elements():
                return(doc.getTextFrames().getElementNames())
            def get_selected_elementname():
                return(doc.getCurrentSelection().getName())
            def jump_to_next_element():
                self.executeDispatch("JumpToNextFrame")
            def delete_selected_element():
                self.executeDispatch("Delete")
        else:
            raise Exception("Unknown Element type %s" % elementtype)

        def get_selected_elementname_save():
            try:
                return(get_selected_elementname())
            except AttributeError:
                pass # not in a element(table/frame/...) yet

        if elementname not in get_all_existing_elements():
            print("Warning: Received instruction to delete nonexisting %s '%s'" %
                    (elementtype, frame_to_delete))
            return

        while get_selected_elementname_save() != elementname:
            jump_to_next_element()

        delete_selected_element()


    def delete_lhs_until_nonwhitespacechar(self):
        while not self.doc.getCurrentController().ViewCursor.getString().rstrip('\n'):
            self.executeDispatch("GoLeft", { 'Count': 1, 'Select': True })
        self.executeDispatch("GoRight", { 'Count': 1, 'Select': True })
        self.executeDispatch("SwBackspace")


def translate(string, lang):
    return _translate(string, lang)


def render_source(lodoc, recipe_meta, source_name, page=None, as_reference=False):
    if as_reference:
        lodoc.lo_insert_text(translate("see", recipe_meta['language']) + ":")
    else:
        lodoc.executeDispatch("Bold", {'Bold': True})
        lodoc.lo_insert_text(translate("Source", recipe_meta['language']))
        lodoc.executeDispatch("Bold", {'Bold': False})
        lodoc.lo_insert_text(': ')
    lodoc.executeDispatch("InsertNonBreakingSpace")
    lodoc.lo_gen_anchor_ref(source_name)
    if page is not None:
        lodoc.lo_insert_text(" (" + translate("page", recipe_meta['language']) + ":")
        lodoc.executeDispatch("InsertNonBreakingSpace")
        lodoc.lo_insert_text( "%s)" % page)


def add_used_bibliography(source_name, used_bibliography, local_bib_definition):
    global_bib_definition = None
    if source_name in ARGS.bibliography.keys():
        global_bib_definition = ARGS.bibliography[source_name]

    if source_name not in used_bibliography.keys():
        used_bibliography[source_name] = {}
    target_definition = used_bibliography[source_name]

    all_dicts = [target_definition, local_bib_definition, global_bib_definition]
    dicts_to_compare = [_dict for _dict in all_dicts if _dict is not None]
    for entry in ['type', 'url', 'title', 'isbn']:
        definitions = [ doc.get(entry) for doc in dicts_to_compare ]
        uniq_nonnull_def = set(definitions) - set([None])
        if len(uniq_nonnull_def) == 1:
            target_definition[entry] = list(uniq_nonnull_def)[0]
        elif len(uniq_nonnull_def) > 1:
            raise Exception("Ambiguous definitions for bib '%s': %s" %
                    (entry, uniq_nonnull_def ))


def render_recipe_source(lodoc, recipe_meta, used_bibliography):
    recipe = recipe_meta['content']
    source = recipe['source']
    try:
        page = source['page']
    except:
        page = None
    local_bib_definition = None

    if isinstance(source, str):
        source_name = source
    else:
        local_bib_definition = source
        if 'name' in source.keys():
            source_name = source['name']
        else:
            raise Exception("Missing source name specification in recipe '%s'" % recipe['name'])

    add_used_bibliography(source_name, used_bibliography, local_bib_definition)

    render_source(lodoc, recipe_meta, source_name, page)


def create_bottom_imagepos_table(lodoc):
    lodoc.go_to_end_of_doc()
    lodoc.lo_insert_text("\t") # add canery
    lodoc.executeDispatch("GoLeft")
    lodoc.executeDispatch("InsertTable", {"TableName": "imagepos_bottom", "Columns": 1, "Rows": 1, "Flags": 8})
    lodoc.lo_insert_text(" ") # to enter table
    lodoc.saveinsert_bookmark("imagepos_bottom")

def remove_bottom_imagepos_table(lodoc):
    lodoc.executeDispatch("DeleteRows")
    lodoc.executeDispatch("Delete") # tabcanery

def calc_bottom_pic_placement(pic_conf, lodoc, placement_list):
    doc = lodoc.doc
    pagecount_at_beginning = doc.getCurrentController().PageCount
    create_bottom_imagepos_table(lodoc)

    upper_y_pos = lodoc.doc.getCurrentController().ViewCursor.Position.Y
    lower_y_pos = 0
    def scale_font_and_insert_paras_til_page_exeeds(size):
        nonlocal lower_y_pos
        lodoc.executeDispatch("FontHeight", {"FontHeight.Height": size})
        while pagecount_at_beginning == doc.getCurrentController().PageCount:
            lower_y_pos = lodoc.doc.getCurrentController().ViewCursor.Position.Y
            lodoc.executeDispatch("InsertPara")
    scale_font_and_insert_paras_til_page_exeeds(20)
    lodoc.executeDispatch("SwBackspace")
    scale_font_and_insert_paras_til_page_exeeds(2)
    size_y_max = lower_y_pos - upper_y_pos
    if size_y_max < 400:
        remove_bottom_imagepos_table(lodoc)
        return

    # clear table content
    lodoc.executeDispatch("SelectAll")
    lodoc.executeDispatch("SwBackspace")
    #lodoc.lo_insert_text(" ") # to reenter table

    wide_pic_filepath = convertPathToOOPath(
        os.path.join(SCRIPTDIR, 'templates/wide_pic.png'))
    args={
            'filename': wide_pic_filepath,
            'bookmark': "imagepos_bottom",
            }
    new_image_name = lodoc.put_graphic_on_bookmarklocation(**args)
    new_image = doc.getGraphicObjects().getByName(new_image_name)
    size_x_max = new_image.Width
    lodoc.executeDispatch("SwBackspace") # delete the graphic

    filepath = convertPathToOOPath(pic_conf['actual_picture_path'])
    args={
            'filename': filepath,
            'bookmark': "imagepos_bottom",
            }
    new_image_name = lodoc.put_graphic_on_bookmarklocation(**args)
    new_image = doc.getGraphicObjects().getByName(new_image_name)
    size_x_actual = new_image.Width
    size_y_actual = new_image.Height
    req_shrink_factor_x = size_x_max / size_x_actual
    req_shrink_factor_y = size_y_max / size_y_actual
    if req_shrink_factor_x >=1 and req_shrink_factor_y >=1:
        resize_factor = 1
    else:
        if req_shrink_factor_x < req_shrink_factor_y:
            resize_factor = req_shrink_factor_x
        else:
            resize_factor = req_shrink_factor_y
    calculated_size_x = int(size_x_actual * resize_factor)
    calculated_size_y = int(size_y_actual * resize_factor)


    placement_list.append({
        'additional_pages': 0,
        'bookmark': 'imagepos_bottom',
        'size': "%d%%" % (resize_factor*100),
        'width': calculated_size_x,
        'height': calculated_size_y,
                })
    remove_bottom_imagepos_table(lodoc)


def estimate_best_place_for_first_pic(pics_conf, doc):
    pic_conf = pics_conf[0]
    filepath = convertPathToOOPath(pic_conf['actual_picture_path'])
    lodoc = LODoc(doc)
    all_imagepos_bookmarks = lodoc.get_all_bookmarks_starts_with('imagepos')
    pagecount_without_picture = doc.getCurrentController().PageCount
    widthdict={'with_pagebreak': {}, 'without_pagebreak': {}}
    placement_list=[]
    for bm in all_imagepos_bookmarks:
        args={
                'filename': filepath,
                'bookmark': bm,
                }
        new_image_name = lodoc.put_graphic_on_bookmarklocation(**args)
        new_image = doc.getGraphicObjects().getByName(new_image_name)

        nonbreaking_size_found = False
        for p in [1, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3]:
            if p == 1:
                initial_picture_width = new_image.Width
                initial_picture_height = new_image.Height
            else:
                new_image.Height = initial_picture_height * p
                new_image.Width = initial_picture_width * p

            pagecount_with_picture = doc.getCurrentController().PageCount
            placement_list.append({
                'additional_pages': pagecount_with_picture - pagecount_without_picture,
                'bookmark': bm,
                'size': "%d%%" % (p*100),
                'width': new_image.Width,
                'height': new_image.Height,
                })
            pagebreak_introduced = pagecount_with_picture > pagecount_without_picture
            if not pagebreak_introduced:
                nonbreaking_size_found = True
                break # the loop if no pagebreaks were introduced
        if nonbreaking_size_found and p != 1:
            # now scale up to 9% to find the optimal size
            for pp in range(1,10):
                new_image.Height = initial_picture_height * (p + pp * .01)
                new_image.Width = initial_picture_width * (p + pp * .01)

                pagecount_with_picture = doc.getCurrentController().PageCount
                placement_list.append({
                    'additional_pages': pagecount_with_picture - pagecount_without_picture,
                    'bookmark': bm,
                    'size': "%d%%" % (p*100),
                    'width': new_image.Width,
                    'height': new_image.Height,
                    })
                pagebreak_introduced = pagecount_with_picture > pagecount_without_picture
                if pagebreak_introduced:
                    break

        lodoc.executeDispatch("Escape")
        new_image.dispose()

    calc_bottom_pic_placement(pic_conf, lodoc, placement_list)

    min_additional_pages = min([ e['additional_pages'] for e in placement_list ])
    placements_with_min_additional_pages = [ e for e in placement_list if e['additional_pages'] == min_additional_pages ]
    biggest_width_within_placement_candidates = max( [e['width'] for e in placements_with_min_additional_pages] )
    placement_candidates_with_biggest_width = [ e for e in placements_with_min_additional_pages if e['width'] == biggest_width_within_placement_candidates  ]
    chosen_placement = placement_candidates_with_biggest_width[0]

    pic_conf['imagepos'] = chosen_placement['bookmark']
    pic_conf['size'] = chosen_placement['size']


def place_pics_on_imagepos(pics_conf, lodoc):
    used_bookmark_locations = []
    all_imagepos_bookmarks = lodoc.get_all_bookmarks_starts_with('imagepos')
    for pic_conf in pics_conf:
        bm = pic_conf['imagepos']
        if not bm.startswith('imagepos_'):
            bm = 'imagepos_' + bm

        if bm == 'imagepos_bottom':
            create_bottom_imagepos_table(lodoc)

        used_bookmark_locations.append(bm)
        #if bm not in all_imagepos_bookmarks:
        #    raise Exception("Bookmark %s not found in Template" % bm)
        args = {
                'filename' : convertPathToOOPath(pic_conf['actual_picture_path']),
                'bookmark' : bm,
                }
        new_image = lodoc.put_graphic_on_bookmarklocation(**args)
        lodoc.executeDispatch("Escape")
        if 'size' in pic_conf:
            if not re.match(r'^[0-9]+%$', str(pic_conf['size'])):
                raise Exception("Unsupported size definition: %s" % pic_conf['size'])
            p = int(pic_conf['size'].rstrip('%')) / 100
            lodoc.doc.getGraphicObjects().getByName(new_image).Width *= p
            lodoc.doc.getGraphicObjects().getByName(new_image).Height *= p

        if bm == 'imagepos_bottom':
            # If table slided down while resizing pic, it may be stuck on the next page.
            lodoc.executeDispatch("SelectAll")
            lodoc.executeDispatch("Cut")
            lodoc.executeDispatch("Paste")

    return used_bookmark_locations


def validate_ingredients(recipe_meta):
    ingredients = recipe_meta['content']['ingredients']
    contained_keys = set(itertools.chain(*[list(i.keys()) for i in ingredients]))
    known_keys = {'amount', 'name', 'note', 'longnote', 'processed', 'unit', 'group', 'reference'}
    unknown_keys = contained_keys - known_keys
    if unknown_keys:
        raise Exception("Unknown ingredient keys: '%s' in recipe '%s'" %
                (unknown_keys, recipe_meta['location']))


def insert_ingredients(recipe_meta, lodoc, used_bibliography, accumulated_footnotes):
    validate_ingredients(recipe_meta)
    recipe = recipe_meta['content']
    if lodoc.jump_to_bookmark_if_it_exists('ingredients_heading'):
        lodoc.lo_insert_text(translate("Ingredients", recipe_meta['language']))
    if lodoc.jump_to_bookmark_if_it_exists('ingredients_list_table'):
        on_pristine_row = True
        for ing in recipe['ingredients']:
            if on_pristine_row:
                on_pristine_row = False
            else:
                lodoc.jump_to_next_tablecell_save()

            if 'group' in ing.keys():
                # move in table to make sure LO realizes that we are operating in a table -.-
                lodoc.executeDispatch("GoRight")
                lodoc.executeDispatch("GoLeft")
                # introduce new row; otherwise all following rows will be merged
                lodoc.executeDispatch("InsertRowsAfter")
                lodoc.executeDispatch("GoRight", { 'Count': 2, 'Select': True, })
                lodoc.executeDispatch("MergeCells")
                lodoc.lo_insert_text(ing['group'])
                lodoc.executeDispatch("LeftPara")
                lodoc.jump_to_next_tablecell_save()
                on_pristine_row = True
            else:
                for ing_t in ['amount', 'unit', 'name']:
                    if ing_t in ing.keys():
                        lodoc.lo_insert_text(ing[ing_t])
                    if ing_t == 'amount':
                        lodoc.executeDispatch("RightPara", {'RightPara': True})
                    if ing_t != 'name':
                        current_tablecell = lodoc.jump_to_next_tablecell_save()
                        if 'ingredientstable_width_reference' in recipe_meta.keys():
                            if recipe_meta['ingredientstable_width_reference'] == current_tablecell:
                                lodoc.saveinsert_bookmark('ingredientstable_width_reference')
                    else: # we are in name col
                        if 'reference' in ing.keys():
                            done = False
                            if 'local' in ing['reference']:
                                anchor = ing['reference']['local']
                                if anchor in ARGS.bookanchors:
                                    lodoc.lo_insert_text(" (siehe Seite %s/" % ARGS.bookanchors[anchor])
                                    lodoc.executeDispatch("InsertField", { 'Type': 13, 'Subtype': 0, 'Name': anchor })
                                    lodoc.lo_insert_text(")")
                                    done = True
                            if not done and 'source' in ing['reference']:
                                source = ing['reference']['source']
                                name = source['name']
                                add_used_bibliography(name, used_bibliography, source)
                                if 'page' in source.keys():
                                    page = source['page']
                                else:
                                    page = None
                                lodoc.lo_insert_text(" (")
                                render_source(lodoc, recipe_meta, name, page, as_reference=True)
                                lodoc.lo_insert_text(")")

                        if 'note' in ing.keys():
                            lodoc.lo_insert_text(" (%s)" % ing['note'])
                        if 'processed' in ing.keys():
                            lodoc.lo_insert_text("✂ %s" % ing['processed'])
                        if 'longnote' in ing.keys():
                            accumulated_footnotes += [ing['longnote']]
                            lodoc.lo_insert_text(" ☞✎%d" % len(accumulated_footnotes))
    if 'ingredientstable_width_reference' in recipe_meta.keys():
        if not lodoc.jump_to_bookmark_if_it_exists('ingredientstable_width_reference'):
            print("Warning: ignorred ingredientstable_width_reference = %s in %s" %
                    (recipe_meta['ingredientstable_width_reference'], recipe_meta['location']))

    lodoc.executeDispatch("SelectAll") # select cell
    lodoc.executeDispatch("SelectAll") # select table
    lodoc.executeDispatch("SetOptimalColumnWidth")

    if 'ingredientstable_width_reference' in recipe_meta.keys():
        lodoc.savedelete_bookmark('ingredientstable_width_reference')


def merge_pics_specifications(recipe_meta):
    pics_meta = recipe_meta['pics'] if 'pics' in recipe_meta.keys() else []
    pics_path = recipe_meta['content']['pics'] if 'pics' in recipe_meta['content'].keys() else []
    if len(pics_meta) > len(pics_path):
        raise Exception("There are more pic-meta-specifications for %s than specified pics" % recipe_meta['location'])

    recipe_folder = os.path.dirname(os.path.realpath(recipe_meta['location']))
    pics_conf = []
    for pic_path, pic_meta in itertools.zip_longest(pics_path, pics_meta):
        if pic_meta == None:
            pic_meta = {}
        pic_realpath = os.path.join(recipe_folder, pic_path)
        if not os.path.isfile(pic_realpath):
            raise Exception("Missing picture specified in recipe %s: %s" %
                    (recipe_meta['location'], pic_realpath))
        picprocess_relevant_attributes = ['crop']
        picprocess_config = {}
        for attr in picprocess_relevant_attributes:
            if attr in pic_meta.keys():
                picprocess_config[attr] = pic_meta[attr]

        if picprocess_config:
            pic_meta['actual_picture_path'] = process_picture(pic_realpath, picprocess_config)
        else:
            pic_meta['actual_picture_path'] = pic_realpath
        pics_conf.append(pic_meta)
    return pics_conf


def process_picture(pic_realpath, picprocess_config):
    picprocess_config['pichash'] = hashlib.md5(open(pic_realpath,'rb').read()).hexdigest()
    processed_pic_name = hashlib.md5(str(picprocess_config).encode('utf8')).hexdigest() + ".png"
    processed_pic_folder = os.path.join('build', 'processed_pics')
    if not os.path.isdir(processed_pic_folder):
        os.mkdir(processed_pic_folder)
    processed_pic_dst = os.path.join(processed_pic_folder, processed_pic_name)
    if not os.path.isfile(processed_pic_dst):
        img = Image.open(pic_realpath)
        w,h = img.size
        if 'crop' in picprocess_config.keys():
            cropspec = picprocess_config['crop']
            if cropspec is None:
                raise Exception("Undefined crop specifications")
            known_crop_attr = ['left', 'right', 'top', 'bottom']
            known_crop_attr_set = set(known_crop_attr)
            actual_crop_attr_set = set(cropspec.keys())
            if not known_crop_attr_set.intersection(actual_crop_attr_set):
                raise Exception("Unspecified crop instruction")
            unknown_crop_specifications = actual_crop_attr_set - known_crop_attr_set
            if unknown_crop_specifications:
                print("Warning: Unknown crop specifications: %s" %
                        str(unknown_crop_specifications))
            c = {}
            for attr in known_crop_attr:
                base = w if attr == 'left' or attr == 'right' else h
                if attr not in cropspec.keys():
                    val = 0
                else:
                    if not re.match(r'^[0-9]+%?$', str(cropspec[attr])):
                        raise Exception("Malformed crop specification '%s'" %
                                cropspec[attr])
                    val = cropspec[attr] if '%' not in str(cropspec[attr]) else int(cropspec[attr].rstrip('%')) / 100 * base
                c[attr] = val if attr == 'left' or attr == 'top' else base - val

            cropped_img = img.crop((c['left'], c['top'], c['right'], c['bottom'],))
            cropped_img.save(processed_pic_dst)
    return (processed_pic_dst)


def recipe_path_to_builddest(recipe_path):
    result = '__'.join(Path(recipe_path).parts)
    result = os.path.splitext(result)[0]
    result = os.path.join('build', result)
    return result


def process_recipe(recipe_meta):
    actually_processed = False
    cachefile_template = recipe_path_to_builddest(recipe_meta['location'])
    if (not os.path.isfile(cachefile_template + '.odt') or
        not os.path.isfile(cachefile_template + '.yaml') or
        ARGS.forcerebuild):
           process_info = actually_process_recipe(recipe_meta)
           actually_processed = True
    with open(cachefile_template + '.yaml', 'r') as f:
        process_info = yaml.safe_load(f.read())
    for bib in process_info['used_bibliography'].keys():
        add_used_bibliography(bib, ARGS.accumulated_bibliography, process_info['used_bibliography'][bib])
    return actually_processed


def actually_process_recipe(recipe_meta):
    recipe = recipe_meta['content']
    used_bibliography = {}

    outputfile = recipe_path_to_builddest(recipe_meta['location']) + '.odt'
    tmp_outputfile = recipe_path_to_builddest(recipe_meta['location']) + '_prep.odt'
    template_filepath = os.path.join(SCRIPTDIR, 'templates/default_recipe.odt')
    shutil.copy(template_filepath, tmp_outputfile)

    template_conf_filepath = template_filepath.rstrip(".odt") + ".yaml"
    if os.path.isfile(template_conf_filepath):
        with open(template_conf_filepath) as f:
            template_conf = yaml.safe_load(f.read())
    else:
        template_conf = {}

    with openDocument(tmp_outputfile) as doc:
        lodoc = LODoc(doc)
        accumulated_footnotes = []

        lodoc.jump_to_bookmark('recipe_title')
        if 'anchor' in recipe.keys():
            lodoc.lo_gen_anchor(recipe['name'], recipe['anchor'])
        else:
            lodoc.lo_insert_text(recipe['name'])


        if ('description' not in recipe.keys() or
            'ignore_description' in recipe_meta.keys() and recipe_meta['ignore_description']):
            if lodoc.jump_to_bookmark_if_it_exists('description'):
                lodoc.executeDispatch("SwBackspace")
        else:
            lodoc.jump_to_bookmark('description')
            for passage in recipe['description'].rstrip('\n').split('\n'):
                lodoc.lo_insert_text(passage)
                lodoc.executeDispatch("InsertPara")
            lodoc.executeDispatch("SwBackspace") #delete last inserted Para


        if lodoc.jump_to_bookmark_if_it_exists("steps_heading"):
            lodoc.lo_insert_text(translate("Preparation", recipe_meta['language']))
        lodoc.jump_to_bookmark("steps")
        very_first = True

        def process_methodlist(methodlist):
            nonlocal very_first
            nonlocal accumulated_footnotes
            first = True
            for method in methodlist:
                if first:
                    first = False
                else:
                    lodoc.executeDispatch("InsertPara")

                if isinstance(method, str):
                    lodoc.lo_insert_text(method)
                elif isinstance(method, dict):
                    if 'content' in method.keys():
                        if 'title' in method.keys():
                            lodoc.lo_insert_text(' ')
                            lodoc.executeDispatch("GoLeft", { 'Count': 1, 'Select': True })
                            lodoc.executeDispatch("Cut")
                            lodoc.executeDispatch("DefaultNumbering", { 'On': False})
                            if not very_first:
                                lodoc.executeDispatch("InsertPara")
                            lodoc.doc.getCurrentController().ViewCursor.ParaKeepTogether = True
                            lodoc.executeDispatch("Bold", {'Bold': True})
                            lodoc.lo_insert_text(method['title'] + ":")
                            lodoc.executeDispatch("Bold", {'Bold': False})
                            lodoc.executeDispatch("InsertPara")
                            lodoc.executeDispatch("Paste")
                            lodoc.executeDispatch("SwBackspace")

                        process_methodlist(method['content'])
                    else:
                        unknown_keys = set(method.keys()) - {'description', 'note'}
                        if unknown_keys:
                            raise Exception("Unknown key-elements '%s' encounted in method while processing recipe '%s'" %(unknown_keys, recipe_meta['location']))
                        if 'description' in method.keys():
                            lodoc.lo_insert_text(method['description'])
                        if 'note' in method.keys():
                            accumulated_footnotes += [method['note']]
                            lodoc.lo_insert_text(" ☞✎%d" % len(accumulated_footnotes))
                else:
                    raise Exception("Unknown method type %s" % type(method))
                if very_first:
                    very_first = False

        process_methodlist(recipe['method'])


        if 'ingredients' not in recipe.keys():
            lodoc.delete_all_elements_with_nameprefixes('ingredients')
        else:
            if 'replace_ingredient_table' not in recipe_meta.keys():
                if lodoc.jump_to_bookmark_if_it_exists("ingredients_upper_relocation"):
                    lodoc.executeDispatch("Delete")
                if lodoc.jump_to_bookmark_if_it_exists("ingredients_lower_relocation"):
                    lodoc.executeDispatch("SwBackspace")
            else:
                lodoc.jump_to_bookmark("ingredients_heading")
                lodoc.executeDispatch("SelectAll")
                lodoc.executeDispatch("Cut")
                if recipe_meta['replace_ingredient_table'] == 'down':
                    if lodoc.jump_to_bookmark_if_it_exists("ingredients_upper_relocation"):
                        lodoc.executeDispatch("Delete")
                    lodoc.jump_to_bookmark("ingredients_lower_relocation")
                elif recipe_meta['replace_ingredient_table'] == 'up':
                    if lodoc.jump_to_bookmark_if_it_exists("ingredients_lower_relocation"):
                        lodoc.executeDispatch("SwBackspace")
                    lodoc.jump_to_bookmark("ingredients_upper_relocation")
                else:
                    raise Exception("Unknown definition for 'replace_ingredient_table' specification: '%s'" % recipe_meta['replace_ingredient_table'])
                lodoc.executeDispatch("Paste")
                tt = lodoc.doc.getTextTables()
                t = tt.getByIndex(tt.getCount() - 1)
                t.setName("ingredients")
                t.RelativeWidth = '100%'
                t.HoriOrient = 3 # orient to the left
                lodoc.doc.getTextFrames().getByName("ingredients").dispose()
            insert_ingredients(recipe_meta, lodoc, used_bibliography, accumulated_footnotes)

        source_is_suppressed = False
        if 'suppress_source_info' in recipe_meta.keys() and recipe_meta['suppress_source_info']:
            source_is_suppressed = True
        source_needs_to_be_rendered = 'source' in recipe.keys() and not source_is_suppressed

        if 'metadata' not in recipe.keys() and not source_needs_to_be_rendered:
            # change focus in case we are catched on Table-Select all
            lodoc.executeDispatch("GoToStartOfPage")
            lodoc.delete_all_elements_with_nameprefixes('infobox')
        elif 'infobox' not in lodoc.doc.getBookmarks().getElementNames():
            print("Error: missing bookmark 'infobox' in template document")
        else:
            lodoc.executeDispatch("JumpToMark", {'Bookmark': 'infobox'})

            prepended_seperator_required = False
            def newline_handler(func):
                if prepended_seperator_required:
                    # Workaround: To get the real linecount we need to add and remove Para
                    lodoc.executeDispatch("InsertPara")
                    lodoc.executeDispatch("SwBackspace")
                    lodoc.saveinsert_bookmark('infobox_before_separator')
                    lodoc.lo_insert_text(' ' * 8)
                    linecount_start = doc.getCurrentController().LineCount
                func()
                if prepended_seperator_required:
                    if linecount_start != doc.getCurrentController().LineCount:
                        lodoc.saveinsert_bookmark('infobox_to_return')
                        lodoc.jump_to_bookmark('infobox_before_separator')
                        for i in range(8):
                            lodoc.executeDispatch("Delete") # remove separator
                        lodoc.executeDispatch("InsertPara")
                        lodoc.jump_to_bookmark('infobox_to_return')
                        lodoc.savedelete_bookmark('infobox_to_return')
                lodoc.savedelete_bookmark('infobox_before_separator')

            if 'metadata' in recipe.keys():
                for (name, value) in recipe['metadata'].items():
                    def add_entry():
                        lodoc.executeDispatch("Bold", {'Bold': True})
                        lodoc.lo_insert_text(name)
                        lodoc.executeDispatch("Bold", {'Bold': False})
                        lodoc.lo_insert_text(': ' + value)
                    if not isinstance(value, str):
                        print("Warning: Ignorring nonsting metainfo %s" % name)
                    else:
                        newline_handler(add_entry)
                        prepended_seperator_required = True
            if source_needs_to_be_rendered:
                newline_handler(lambda: render_recipe_source(lodoc, recipe_meta, used_bibliography))

        def draw_footnotes_and_annotations(footnote_or_annotation, collection):
            nonlocal recipe_meta
            if not collection:
                if lodoc.jump_to_bookmark_if_it_exists(footnote_or_annotation + '_heading'):
                    lodoc.executeDispatch("Delete")
                if lodoc.jump_to_bookmark_if_it_exists(footnote_or_annotation + '_first'):
                    lodoc.executeDispatch("Delete")
            else:
                if lodoc.jump_to_bookmark_if_it_exists(footnote_or_annotation + '_heading'):
                    if footnote_or_annotation == 'annotation':
                        lodoc.lo_insert_text(translate("Annotations", recipe_meta['language']))
                    else:
                        lodoc.lo_insert_text(translate("Footnotes", recipe_meta['language']))
                lodoc.jump_to_bookmark(footnote_or_annotation + '_first')
                for footnote_or_annotation_element in collection:
                    def prepare_string(string):
                        return '\n\n'.join(string.split('\n')).rstrip('\n')
                    if isinstance(footnote_or_annotation_element, str):
                        lodoc.lo_insert_text(prepare_string(footnote_or_annotation_element))
                    else:
                        lodoc.saveinsert_bookmark('start_of_note')
                        lodoc.lo_insert_text(' ') # Add space separator to prevent bulletpoint from getting bold
                        lodoc.executeDispatch("Bold", {'Bold': True})
                        lodoc.lo_insert_text(footnote_or_annotation_element['title'] + ': ')
                        lodoc.executeDispatch("Bold", {'Bold': False})
                        lodoc.lo_insert_text(prepare_string(footnote_or_annotation_element['content']))
                        lodoc.saveinsert_bookmark('end_of_note')
                        lodoc.jump_to_bookmark('start_of_note')
                        lodoc.executeDispatch("Delete") # Remove previously added space
                        lodoc.jump_to_bookmark('end_of_note')
                        lodoc.savedelete_bookmark('start_of_note')
                        lodoc.savedelete_bookmark('end_of_note')
                    lodoc.executeDispatch("InsertPara")
                # delete last point created by last para
                lodoc.executeDispatch("DelToStartOfWord")

        draw_footnotes_and_annotations("footnote", accumulated_footnotes)
        draw_footnotes_and_annotations("annotation", recipe['annotations'] if 'annotations' in recipe.keys() else None)


        used_pic_bookmarks = []
        pics_conf = merge_pics_specifications(recipe_meta)

        # scale last line down to give images more place to grow
        lodoc.go_to_end_of_doc()
        lodoc.executeDispatch("FontHeight", {"FontHeight.Height": 2})

        if pics_conf:
            if 'ignore_pics' in recipe_meta.keys() and recipe_meta['ignore_pics']:
                pass
            else:
                pics_with_explicit_placements = [ pic_conf for pic_conf in pics_conf if 'imagepos' in pic_conf.keys() ]
                if not pics_with_explicit_placements:
                    estimate_best_place_for_first_pic(pics_conf, doc)
                    pics_with_explicit_placements.append(pics_conf[0])
                used_pic_bookmarks = place_pics_on_imagepos(pics_with_explicit_placements, lodoc)
        pic_bookmarks = lodoc.get_all_bookmarks_starts_with('imagepos')
        unused_bookmarks = [ b for b in pic_bookmarks if b not in used_pic_bookmarks ]
        lodoc.delete_all_elements_with_nameprefixes(unused_bookmarks)


        if 'pull_ingredientframe_down' in recipe_meta.keys() and recipe_meta['pull_ingredientframe_down']:
            if doc.getCurrentController().PageCount == 1:
                print("Ignorring 'pull_ingredientframe_down' instruction on single page")
            else:
                doc.getTextFrames().getByName('ingredients').dispose()
                lodoc.executeDispatch("Undo")
                lodoc.executeDispatch("Cut")
                lodoc.executeDispatch("JumpToHeader") # in case we ended in footnote section after cut
                if doc.getCurrentController().PageCount == 1:
                    lodoc.saveinsert_bookmark("recipe_title")
                    lodoc.executeDispatch("GoToEndOfPage")
                    # Bugfix: one Pagebreak will be lost in copy-past process further down the road
                    lodoc.executeDispatch("InsertPagebreak")
                    lodoc.executeDispatch("InsertPagebreak")
                    lodoc.saveinsert_bookmark("tmp_for_jumpback")
                    lodoc.executeDispatch("GoLeft")
                    lodoc.executeDispatch("GoLeft")
                    # remove all whitespace before pagebreak
                    lodoc.delete_lhs_until_nonwhitespacechar()
                    lodoc.jump_to_bookmark("tmp_for_jumpback")
                    lodoc.savedelete_bookmark("tmp_for_jumpback")
                    # Add Marker to prevent Pagebreak deletion in chapter-collection-process
                    lodoc.lo_insert_text('\t')
                else:
                    lodoc.executeDispatch("GoToStartOfDoc")
                    lodoc.executeDispatch("GoToStartOfNextPage")
                lodoc.executeDispatch("Paste")


        lodoc.savedelete_all_bookmarks()

        cwd = systemPathToFileUrl( os.getcwd() )
        destFile = absolutize( cwd, systemPathToFileUrl(tmp_outputfile) )
        doc.storeAsURL(destFile, ())

        with open(recipe_path_to_builddest(recipe_meta['location']) + '.yaml', 'w') as f:
            f.write(yaml.dump({'used_bibliography': used_bibliography}))
    shutil.move(tmp_outputfile, outputfile)


def copy_content_from_document(document_path):
    with openDocument(document_path) as doc:
        lodoc = LODoc(doc)

        # Go back to start to make sure the selection selects all; not just content of the table the cursor is currently in
        lodoc.executeDispatch("GoToStartOfPage")
        lodoc.executeDispatch("SelectAll")
        lodoc.executeDispatch("SelectAll")
        lodoc.executeDispatch("SelectAll")
        lodoc.executeDispatch("Copy")
        time.sleep(.5) # unfortunately necessary to really copy the whole content


def load_yaml(location):
    with open(location) as f:
        return yaml.safe_load(f.read())


def main():
    parser = argparse.ArgumentParser(
            prog='gen_recipebook',
            description='Translates a recipe-collection described in a yaml file to a doc book')
    parser.add_argument('recipe_config_path', type=str)
    parser.add_argument('--language', action='store', required=False)
    parser.add_argument('--forcerebuild', action='store_true')
    parser.add_argument('--suppress-source-info', action='store_const', required=False, const=True)
    global ARGS
    ARGS = parser.parse_args()

    ARGS.bookanchors = {}
    ARGS.accumulated_bibliography = accumulated_bibliography = {}
    bookconfig = load_yaml(ARGS.recipe_config_path)

    # from now on we want to operate relative to the directory of the recipe_config_path
    os.chdir(os.path.dirname(os.path.realpath(ARGS.recipe_config_path)))

    if not os.path.isdir('build'):
        os.mkdir('build')

    try:
        ARGS.bibliography = load_yaml(bookconfig['bibliography']['location'])
    except KeyError:
        ARGS.bibliography = None
    chapters = bookconfig['chapters']


    for chapter in chapters:
        chapter['recipe_collection'] = []
        for recipe_meta_or_name in chapter['recipes']:
            if isinstance(recipe_meta_or_name, str):
                recipe_meta = {'location': recipe_meta_or_name}
            else:
                recipe_meta = recipe_meta_or_name
            if 'formating' in bookconfig.keys():
                if recipe_meta['location'] in bookconfig['formating'].keys():
                    global_config_for_this_recipe = bookconfig['formating'][recipe_meta['location']]
                    for key in global_config_for_this_recipe.keys():
                        if key not in recipe_meta.keys():
                            recipe_meta[key] = global_config_for_this_recipe[key]
            chapter['recipe_collection'].append(recipe_meta)

            for entry in ['language', 'suppress_source_info', 'replace_ingredient_table']:
                if hasattr(ARGS, entry) and getattr(ARGS, entry) is not None:
                    bookconfig[entry] = recipe_meta[entry] = getattr(ARGS, entry)
                elif entry not in recipe_meta.keys():
                    if entry in bookconfig.keys():
                        recipe_meta[entry] = bookconfig[entry]
                    else: # define defaults
                        if entry == 'language':
                            bookconfig[entry] = recipe_meta[entry] = 'en'


            recipe = load_yaml(recipe_meta['location'])
            recipe_meta['content'] = recipe
            if 'anchor' in recipe.keys():
                if recipe['anchor'] in ARGS.bookanchors.keys():
                    raise Exception("Multiple definitions for anchor '%s'" % recipe['anchor'])
                ARGS.bookanchors[recipe['anchor']] = chapter['name']

    at_least_one_chapter_rerendered = False
    for chapter in chapters:
        chapter_storefile = os.path.join('build', 'chapter_' + chapter['name'] + '.odt')
        tmp_chapter_storefile = os.path.join('build', 'chapter_' + chapter['name'] + '_prep.odt')
        chapter['chapter_storefile'] = chapter_storefile

        at_least_one_recipe_rerendered = False
        for recipe_meta in chapter['recipe_collection']:
            if process_recipe(recipe_meta):
                at_least_one_recipe_rerendered = True

        if at_least_one_recipe_rerendered or not os.path.isfile(chapter_storefile):
            at_least_one_chapter_rerendered = True
            if 'name' not in chapter.keys():
                raise Exception("Missing namespecification for chapter")
            shutil.copy(os.path.join(SCRIPTDIR, 'templates/default_chapter.odt'), tmp_chapter_storefile)
            first_recipe = True
            for recipe_meta in chapter['recipe_collection']:
                copy_content_from_document(recipe_path_to_builddest(recipe_meta['location']) + '.odt')
                with openDocument(tmp_chapter_storefile) as doc:
                    lodoc = LODoc(doc)
                    if first_recipe:
                        lodoc.jump_to_bookmark("chapter_title")
                        lodoc.lo_insert_text(chapter['name'])
                    lodoc.jump_to_bookmark("recipe")
                    lodoc.savedelete_bookmark('recipe')
                    lodoc.saveinsert_bookmark('recipe_current')
                    if not first_recipe:
                        lodoc.executeDispatch("GoLeft")
                        lodoc.delete_lhs_until_nonwhitespacechar()
                        lodoc.jump_to_bookmark('recipe_current')
                    pagecount_before_paste = doc.getCurrentController().PageCount
                    pagebreak_added = False
                    if 'start_recipe_on_new_page' in recipe_meta.keys() and recipe_meta['start_recipe_on_new_page']:
                        lodoc.executeDispatch("InsertPagebreak")
                        pagebreak_added

                    lodoc.lo_insert_text(" ") # shake off bookmark
                    lodoc.executeDispatch("InsertPara")
                    lodoc.executeDispatch("Paste")
                    lodoc.saveinsert_bookmark('recipe')
                    lodoc.jump_to_bookmark("recipe_current")
                    lodoc.executeDispatch("Delete") # remove space (and bm as byproduct)
                    lodoc.executeDispatch("Delete") # remove added linebreak
                    lodoc.saveinsert_bookmark('recipe_current')
                    pagecount_after_paste = doc.getCurrentController().PageCount
                    if pagecount_before_paste != pagecount_after_paste and not pagebreak_added:
                        # pagebreak will be introduced anyway
                        # lets add an explicit Pagebreak
                        lodoc.jump_to_bookmark("recipe_current")
                        lodoc.executeDispatch("InsertPagebreak")
                        lodoc.executeDispatch("GoLeft")
                        # remove all whitespace before pagebreak
                        lodoc.delete_lhs_until_nonwhitespacechar()
                    lodoc.savedelete_bookmark('recipe_current')
                    lodoc.jump_to_bookmark("recipe")
                    lodoc.executeDispatch("InsertPara")
                    lodoc.executeDispatch("UpdateAllIndexes")
                    cwd = systemPathToFileUrl( os.getcwd() )
                    destFile = absolutize( cwd, systemPathToFileUrl(tmp_chapter_storefile) )
                    doc.storeAsURL(destFile, ())
                    if first_recipe:
                        first_recipe = False
            shutil.move(tmp_chapter_storefile, chapter_storefile)

    outputfile = 'build/book.odt'
    tmp_outputfile = 'build/book_prep.odt'
    if at_least_one_chapter_rerendered or not os.path.isfile(outputfile):
        shutil.copy(os.path.join(SCRIPTDIR, 'templates/default_book.odt'), tmp_outputfile)

        chapter_count = len(chapters)
        for chapter_num, chapter in enumerate(chapters):
            copy_content_from_document(chapter['chapter_storefile'])
            with openDocument(tmp_outputfile) as doc:
                lodoc = LODoc(doc)
                lodoc.jump_to_bookmark("chapter")
                lodoc.savedelete_bookmark('chapter')
                lodoc.saveinsert_bookmark('chapter_start')
                lodoc.lo_insert_text(" ")
                lodoc.executeDispatch("Paste")
                lodoc.saveinsert_bookmark('chapter')
                lodoc.jump_to_bookmark("chapter_start")
                lodoc.executeDispatch("Delete") # remove space
                lodoc.executeDispatch("SwBackspace") # merge chapters
                lodoc.executeDispatch("SwBackspace") # merge chapters

                if chapter_num + 1 == chapter_count:
                    # last recipe
                    # last two linebreaks may cause the start of a new and empty page
                    lodoc.jump_to_bookmark("chapter")
                    lodoc.executeDispatch("SwBackspace")
                    lodoc.executeDispatch("SwBackspace")

                    lodoc.jump_to_bookmark("book_title")
                    lodoc.lo_insert_text(bookconfig['book_title'])
                    if 'book_subtitle' in bookconfig.keys():
                        if lodoc.jump_to_bookmark_if_it_exists('book_subtitle'):
                            lodoc.lo_insert_text(bookconfig['book_subtitle'])
                    else:
                        if lodoc.jump_to_bookmark_if_it_exists('book_subtitle'):
                            lodoc.executeDispatch("SwBackspace")

                    if not ARGS.accumulated_bibliography:
                        if lodoc.jump_to_bookmark_if_it_exists('bibliography_first'):
                            lodoc.executeDispatch("DelToStartOfWord")
                            lodoc.executeDispatch("Delete")
                    else:
                        lodoc.jump_to_bookmark('bibliography_heading')
                        lodoc.lo_insert_text(translate("Bibliography", bookconfig['language']))
                        lodoc.jump_to_bookmark('bibliography_first')

                        for is_last, (bib, bibentry) in signal_last(ARGS.accumulated_bibliography.items()):
                            LODoc(doc).lo_gen_anchor(bib, bib)
                            lodoc.lo_insert_text(": ")
                            if 'type' not in bibentry.keys():
                                raise Exception("Unspecified bibentry type for '%s'" % bib)
                            if bibentry['type'] == 'book':
                                if 'title' not in bibentry.keys():
                                    raise Exception("Unknown book title for '%s'" % bib)
                                lodoc.lo_insert_text(bibentry['title'])
                                if 'isbn' in bibentry.keys():
                                    lodoc.lo_insert_text("\tISBN: %s" % bibentry['isbn'])
                            elif bibentry['type'] == 'url':
                                if 'url' not in bibentry.keys():
                                    raise Exception("Missing url specification for '%s'" % bib)
                                url = bibentry['url']
                                lodoc.executeDispatch("SetHyperlink", { 'Hyperlink.Text': url, 'Hyperlink.URL': url })
                                # shake off focus
                                lodoc.executeDispatch("GoLeft")
                                lodoc.executeDispatch("GoRight")
                            else:
                                raise Exception("Unknown bibentry type '%s' for '%s'" % (bibentry['type'], bib))


                            if not is_last:
                                lodoc.executeDispatch("InsertPara")

                lodoc.executeDispatch("UpdateAllIndexes")
                lodoc.toggle_fieldnames_view() # Update crossreferences
                cwd = systemPathToFileUrl( os.getcwd() )
                destFile = absolutize( cwd, systemPathToFileUrl(tmp_outputfile) )
                doc.storeAsURL(destFile, ())
        shutil.move(tmp_outputfile, outputfile)


if __name__ == '__main__':
    main()
