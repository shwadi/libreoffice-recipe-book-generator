translations = {}
translations['Ingredients'] = {
        'de': 'Zutaten',
    }

translations['Preparation'] = {
        'de': 'Zubereitung',
    }

translations['Footnotes'] = {
        'de': 'Fußnotizen',
    }

translations['Annotations'] = {
        'de': 'Anmerkungen',
    }
translations['Source'] = {
        'de': 'Quelle',
    }
translations['Bibliography'] = {
        'de': 'Literaturverzeichnis',
    }
translations['see'] = {
        'de': 'siehe',
    }
translations['page'] = {
        'de': 'Seite',
    }

def translate(string, lang):
    try:
        return translations[string][lang]
    except:
        return string
