#!/usr/bin/env bash

set -e

cd "$(dirname "$0")"

SCRIPTNAME=$(basename "$0")
CONFIGFILE=buildlauncher.conf

die(){ echo "$*" >/dev/stderr; exit 1; }

validate_config(){
    # check if all expected variables are defined
    for var in \
	buildenvironment \
	recipe_generator_path \
	cookbook_configfile_path \
	prune_modification_registry; do
	[ -v "$var" ] || die "Invalid configfile "$CONFIGFILE": Variable '$var' is undefined!"
    done

    # validate buildenvironment variable
    if ! grep -qE '^(local|vm_with_local_x11|vm_with_forwarded_x11)$' <<< "$buildenvironment"; then
	die "unsupported value '$buildenvironment' defined for 'buildenvironment' in '$CONFIGFILE'"
    fi
}

wait_until_soffice_gets_ready(){
    if [ -z "$(which ss)" ]; then
	# no ss; simply perform a short sleep and hope for the best
	sleep 2
    else
	local output_generated=false
	until ss -tlp | grep -q soffice; do
	    sleep 1
	    if $output_generated; then
		echo -n .
	    else
		echo -n "Waiting for soffice to get ready ..."
		output_generated=true
	    fi
	done
	$output_generated && echo ' soffice is ready' || true #terminate line if started
    fi
}

main(){
    [ -f "$CONFIGFILE" ] || die "Configfile '$CONFIGFILE' not found! Read the README.md file for more info."
    . "$CONFIGFILE"
    validate_config

    if [ "$(whoami)" == vagrant ] || [ "$buildenvironment" == "$local" ]; then
	pkill -f soffice.*accept=socket || true # kill potentially running instances

	if [ "$prune_modification_registry" == true ]; then
	    rm -f ~/.config/libreoffice/4/user/registrymodifications.xcu
	fi

	soffice --writer --accept='socket,host=localhost,port=2002;urp;StarOffice.Service' &
	wait_until_soffice_gets_ready
	python3 -u "$recipe_generator_path" "$cookbook_configfile_path"
	pkill -f soffice.*accept=socket

	cd "$(dirname "$cookbook_configfile_path")"/build
	rm -f book.pdf
	soffice --headless --convert-to pdf book.odt --outdir .
    else
	if [ "$buildenvironment" == vm_with_local_x11 ]; then
	    cd vagrant_build_environments/x11_local/
	else
	    cd vagrant_build_environments/x11_forward/
	fi

	vagrant up
	# vagrant refuses to pass the -i option directly to the initial bash instance
	# this option is required to source ~/.bashrc and thereby define DISPLAY
	vagrant ssh -c bash -c "bash -i /vagrant_data/$SCRIPTNAME"
    fi
}

main
