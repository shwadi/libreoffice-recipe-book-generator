# LibreOffice recipe book generator

## Purpose of this repository

The primary purpose of this repository is to share the lessons learned during the task of creating a recipe book in an automated matter using libreoffice (LO), python and Unified Network Objects (UNO).

After facing the immense disadvantages described down below, I glued everything together in a quick and dirty way to complete the task and fulfill my personal needs.

## The goal

The goal followed by this project is to generate recipe book with is primarily not intended to be inspected digitally but rather be printed and stored in a document folder.

The chapters (thus recipe categories) would be visually separated in the document folder and are meant to be extended over time.
Therefore each chapter
* needs to reset the page count
* would require its own table of contents

In theory each chapter could be handled as an individual book, but combining them in one final document would
  * be handy for several reasons
  * allow cross references to other recipes between chapters

## Investigation of available solutions

* [cookybooky](https://duckduckgo.com/?t=ffab&q=cookybooky&ia=web) and variations like [xcookybooky](https://github.com/SvenHarder/xcookybooky)
  * description: LaTeX package
  * [opensource example UNO recipebook](https://github.com/StevenHickson/Recipe-Book) utilizing xcookybooky
  * disadvantages:
    * limited design features
    * It is based on LaTeX!
      Thus simple tasks like breaking an log ingredient collection on multiple pages is a
      [pain in the ass](https://stackoverflow.com/questions/64688876/using-xcookybooky-in-latex-i-have-many-ingredients-but-it-does-go-into-the-nex).
    * Recipe description is latex-based too and thus not easy portable to alternative solutions.

* [pandoc-cookbook](https://github.com/keeferrourke/pandoc-cookbook) or [pandoc-recipe-template](https://github.com/iwismer/pandoc-recipe-template)
  * description: typesets recipes defined in yaml using Pandoc as a front-end for LaTeX
  * advantages:
    * recipes are written in yaml and are thus easy portable
    * no need to mess around with LaTeX (in theory)
  * disadvantages:
    * limited design features
    * design not very approachable (subjective assessment)

* [awesome-cookbook](https://github.com/latexstudio/awesome-cookbook)
  * description: LaTeX/[LuaTeX](http://luatex.org/) template for cookbooks
  * advantages:
    * quite decent design (subjective assessment)
  * disadvantages:
    * recipes are written in LaTeX (nonetheless with limited ugliness due to the reuse of predefined commands)
    * full body contact with LaTeX is inevitable (if I remember right, I was confronted with it as soon as my ingredients extended the limited table space)

* [recipes-notion-to-pdf](https://github.com/seb-ma/recipes-notion-to-pdf)
  * description: npm based recipe book generator
  * disadvantages:
    * recipes need to be stored in notion db
    * no minimal working example given
    * [example image](https://github.com/seb-ma/recipes-notion-to-pdf/blob/main/sample-recipe.jpg) looks not very feature rich

I also stumbles upon some site generators which did not match my goal but are worth mentioning:

* [nyum](https://github.com/doersino/nyum) [example](https://doersino.github.io/nyum/_site/index.html)
  * description: Pandoc-powered static site generator for markdown-formatted recipes
* [recipe-book](https://github.com/aryan-programmer/recipe-book)

## The solution

I did not want to mess around with LaTeX and decided in the end to build the document with libreoffice.
A web search on how to automate libreoffice document creation led me to UNO which in tern led me to [PyUNO](https://www.openoffice.org/udk/python/python-bridge.html).

The key functionality would be:

* recipes are described as yaml configs (see [example-cookbook](./example-cookbook/))
* each recipe, chapter and the book itself is predefined in an `odt` template document [stored here](./src/templates/)
* first each recipe is build by opening the recipe template and filling it with data
* then the chapter is build by reopening each recipe document and copy-pasting it into the chapter template
* finally the book is build by reopening the chapters and copy-pasting them into the final document
* because of slowness and non reproducible behavior (see [Disadvantages](#disadvantages)) a caching mechanism was required as well
  * thus it is possible to rebuild recipes/chapters/the book by deleting the related parts of the build folder

## Usage

I found it to be the best solution to run PyUNO against a LO instance in a virtual environment, because ...
* it is easy to launch the LO version against which have been developed/tested
* crashes do not affect local LO instances potentially in use for productive purposes

For this reason two setups for [vagrant build environments](vagrant_build_environments/) are shipped with this repo:
* one forwarding X11 to the host via ssh
  * useful for debugging purposes
* one launching a lightweight xserver locally
  * useful for the actual build process which do not annoy with popping up LO windows

To build the example recipe it is sufficient to:
* copy `buildlauncher.conf.example` to `buildlauncher.conf`
* run `buildlauncher.sh`

## Disadvantages

* very slow (building the [simple example book](example-cookbook/book.pdf) takes longer than 1 1/2 min)
  * not least the graphical launch and termination of each single recipe is immense time consuming
    * in theory UNO does not require LO to run in the foreground
      * but this script utilizes features which imply this requirement
      * to launch LO in background anyway:
        * set `Hidden` to `True` in [thirdparty.py](src/thirdparty.py)
        * launch soffice with the options `--headless --nologo` in [buildlauncher.sh](buildlauncher.sh)
  * the fill-in process is slow as well, because hard-coded sleeps were required (see down below)
* non reproducible results
  * if the dispatcher queue is loaded with too many instructions it tend to quietly drop them
    * I did not find a way to query the state of the dispatcher
    * the only solution I came across searching the web is to add hard coded sleep intervals
  * unintended user intervention
    * as the build process runs in the foreground, it is quite likely that unintended user interventions will mess up the build process
* non declarative
  * the iterative document composition description is not flexible as changes in the process flow can easily introduce minor bugs which compound to major hardly back traceable errors in the subsequent build process
* not future save
  * there are several LO bugs forcing one to build workarounds which may be obsolete or insufficient in future versions

## Resources

* UNO-related
  * [thebiasplanet](https://thebiasplanet.blogspot.com/2018/03/thetableofcontentsoftheseriesexploitinganopensourceofficesuite.html)
    * quite comprehensive and entertaining discussion on UNO
  * [DispatchCommands](https://wiki.documentfoundation.org/Development/DispatchCommands)
    * overview over the dispatcher commands
  * [documenthacker](https://documenthacker.wordpress.com/)
    * hosts a more than a decade old second draft of a book dedicated to the topic on writing long documents with python; UNO; LO/OO
